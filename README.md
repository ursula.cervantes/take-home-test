# Welcome to the Coding Challenge

This **README** file contains the instructions you will need to complete your task in the form of a User Story – the format regularly used by N26 engineers in our weekly sprints.

#### Instructions

- This master branch already contains code which you will use for your task. Please add any new code to a new feature branch. When you’re done with the task, please submit a GitLab merge request to master.

- For the parts of the task which you feel are unclear, we encourage you to make your own assumptions as long as they are documented in your README file. However, if you have a question or concern that is blocking you from completing the work, please reach out to us via email.

---

### User Story

As a N26 user, I want to be able to 

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Acceptance Criteria


## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Good vibes! ✨